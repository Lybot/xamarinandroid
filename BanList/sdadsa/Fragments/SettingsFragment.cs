﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using delrep.Models;
using delrep.Models.DatabaseModels;
using Environment = System.Environment;

namespace delrep.Activities
{
    public class SettingsFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view =  inflater.Inflate(Resource.Layout.SettingsLayout, container, false);
            var exitButton=  view.FindViewById<Button>(Resource.Id.exitButton);
            Switch windowSwitch = view.FindViewById<Switch>(Resource.Id.windowSwitch);
            Switch notificationSwitch = view.FindViewById<Switch>(Resource.Id.notificationSwitch);
            var notificationControlButton = view.FindViewById<Button>(Resource.Id.notificationControlButton);
            var updateDbButton = view.FindViewById<Button>(Resource.Id.updateDbButton);
            var phoneTextView = view.FindViewById<TextView>(Resource.Id.myPhoneTextView);
            var changeNameTextView = view.FindViewById<TextView>(Resource.Id.changeNameTextView);
            var preferences = Application.Context.GetSharedPreferences("token", FileCreationMode.Private);
            try
            {
                var name = preferences.GetString("myName", "");
                var firstName = preferences.GetString("myFirstName", "");
                var fullName = name + " " + firstName;
                if (String.IsNullOrWhiteSpace(fullName))
                    phoneTextView.Text = FormatPhone.FormatNumber(preferences.GetString("myNumber", "Мой номер"));
                else phoneTextView.Text = fullName;
                bool premium = preferences.GetBoolean("premium", false);
                if (!premium)
                {
                    view.FindViewById<LinearLayout>(Resource.Id.premiumLinearLayout).Visibility=ViewStates.Visible;
                    view.FindViewById<TextView>(Resource.Id.premiumTextView).Click += delegate
                    {
                        var alert = new AlertDialog.Builder(Context);
                        alert.SetMessage(
                            "В полной версии программы доступны претензии от других пользователей. \nДобавляются уведомления при звонке от агентов, которых другие пользователи считают проблемными\n " +
                            "Для приобретения полной версии обращаться в what's app:\n +7 (913) 827-3743");
                        alert.SetTitle("Бесплатная версия");
                        alert.SetPositiveButton("OK", delegate { });
                        alert.SetNegativeButton("Скопировать номер", delegate
                        {
                            ClipboardManager clipboard = (ClipboardManager)Context.GetSystemService(Context.ClipboardService);
                            ClipData clip = ClipData.NewPlainText("", "79138273743");
                            clipboard.PrimaryClip = clip;
                        });
                        var window = alert.Create();
                        window.Show();
                    };
                }

            }
            catch (Exception)
            {
                phoneTextView.Text = "Мой номер";
            }
            //nameTextView.Text = preferences.GetString("myName", "Мое имя");
            ComponentName windowReceiver = new ComponentName(Application.Context, "com.app.IncomingCallReceiver");
            ComponentName notificationReceiver = new ComponentName(Application.Context, "com.app.IncomingCallReceiverNotification");
            PackageManager pm = Application.Context.PackageManager;
            changeNameTextView.Click += delegate
            {
                var intent = new Intent(Activity, typeof(ChangeSettingActivity));
                StartActivity(intent);
            };
            exitButton.Click += async delegate
            {
                var dbFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                var dbName = "context_db.db";
                var db = new CommentsDb(Path.Combine(dbFolderPath, dbName));
                await db.DeleteDb();
                var editPreferences = preferences.Edit();
                editPreferences.Clear();
                editPreferences.Commit();
                var intent = new Intent(Application.Context,typeof(LoginActivity));
                Activity.Finish();
                StartActivity(intent);
            };
            notificationControlButton.Click += delegate
            {
                var notificationLayout = view.FindViewById(Resource.Id.notificationLinearLayout);
                if (notificationLayout.Visibility == ViewStates.Gone)
                {
                    notificationLayout.Visibility = ViewStates.Visible;
                    var windowState= pm.GetComponentEnabledSetting(windowReceiver);
                    if (windowState == ComponentEnabledState.Enabled)
                        windowSwitch.Checked = true;
                    else
                    {
                        windowSwitch.Checked = false;
                    }
                    var notificationState = pm.GetComponentEnabledSetting(notificationReceiver);
                    if (notificationState == ComponentEnabledState.Enabled)
                        notificationSwitch.Checked = true;
                    else
                    {
                        notificationSwitch.Checked = false;
                    }
                }
                else notificationLayout.Visibility = ViewStates.Gone;
                string[] needPermissions = { Manifest.Permission.ReadPhoneState, Manifest.Permission.ReadCallLog, Manifest.Permission.SystemAlertWindow, Manifest.Permission.InternalSystemWindow };
                if (!(((int)Build.VERSION.SdkInt < 23) ||
                      (Application.Context.CheckSelfPermission(Manifest.Permission.ReadPhoneState) == Permission.Granted)))
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(Activity);
                    alert.SetTitle("Требуются разрешения");
                    alert.SetMessage("Для уведомления о проблемном контрагенте требуется разрешение на телефон");
                    alert.SetPositiveButton("Хорошо", (senderAlert, args) =>
                    {
                        RequestPermissions(needPermissions, 0);
                    });
                    Dialog dialog = alert.Create();
                    dialog.Show();
                }
            };
            notificationSwitch.CheckedChange += delegate
            {
                if (notificationSwitch.Checked == false)
                {
                    pm.SetComponentEnabledSetting(notificationReceiver,ComponentEnabledState.Disabled,ComponentEnableOption.DontKillApp);
                }
                else
                {
                    pm.SetComponentEnabledSetting(notificationReceiver,ComponentEnabledState.Enabled,ComponentEnableOption.DontKillApp);
                }
            };
            windowSwitch.CheckedChange += delegate
            {
                if (windowSwitch.Checked == false)
                {
                    pm.SetComponentEnabledSetting(windowReceiver,ComponentEnabledState.Disabled,ComponentEnableOption.DontKillApp);
                }
                else
                {
                    pm.SetComponentEnabledSetting(windowReceiver,ComponentEnabledState.Enabled,ComponentEnableOption.DontKillApp);
                }
            };
            updateDbButton.Click += delegate
            {
                updateDbButton.Enabled = false;

                var mDialog = new AlertDialog.Builder(Activity);
                View viewDialog = View.Inflate(Application.Context, Resource.Layout.ProgressDialogLayout, null);
                var dialogWindow = mDialog.Create();
                viewDialog.FindViewById<TextView>(Resource.Id.textView).Text = "Обновление списка...";
                dialogWindow.SetView(viewDialog);
                dialogWindow.Window.AddFlags(WindowManagerFlags.NotTouchable
                );
                dialogWindow.Show();
                new Thread(new ThreadStart(async delegate
                {
                    bool success = false;
                    try
                    {
                        var dbFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                        var dbName = "context_db.db";
                        var db = new CommentsDb(Path.Combine(dbFolderPath, dbName));
                        await db.FullUpdateDb(Path.Combine(dbFolderPath, dbName), preferences.GetString("token", ""));                        
                        success = true;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                    Activity.RunOnUiThread(() =>
                    {
                        if (success)
                        {
                            updateDbButton.Enabled = true;
                            Toast.MakeText(Activity, "База обновлена", ToastLength.Long);
                            dialogWindow.Dismiss();
                        }
                        else
                        {
                            updateDbButton.Enabled = true;
                            Toast.MakeText(Activity, "При обновлении базы произошла ошибка", ToastLength.Long);
                            dialogWindow.Dismiss();
                        }
                    });
                })).Start(); ;
            };
            return view;
        }
    }
}