﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace delrep.Fragments
{
    public class SearchFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            var view= inflater.Inflate(Resource.Layout.SearchLayout, container, false);
            var fontBold = Typeface.CreateFromAsset(Application.Context.Assets, "PT_Sans-Web-Bold.ttf");
            var font = Typeface.CreateFromAsset(Application.Context.Assets, "PT_Sans-Web-Regular.ttf");
            var searchEditText = view.FindViewById<EditText>(Resource.Id.searchEditText);
            var searchTextView = view.FindViewById<TextView>(Resource.Id.searchTextView);
            var searchHeadTextView = view.FindViewById<TextView>(Resource.Id.searchHeadTextView);
            var inDevelopTextView = view.FindViewById<TextView>(Resource.Id.inDevelopTextView);
            inDevelopTextView.Typeface = fontBold;
            searchHeadTextView.Typeface = fontBold;
            searchEditText.Typeface = font;
            searchTextView.Typeface = font;
            return view;
        }
    }
}