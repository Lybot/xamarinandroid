﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using delrep.Models;
using delrep.Models.DatabaseModels;
using delrep.Models.PhoneInfoList;
using Environment = System.Environment;
using Path = System.IO.Path;

namespace delrep.Activities
{
    public class PhoneInfoFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View view = inflater.Inflate(Resource.Layout.ProblemPhonesLayout,container, false);
            var phoneTextView = view.FindViewById<TextView>(Resource.Id.problemText);
            var addCommentButton = view.FindViewById<Button>(Resource.Id.addCommentButton);
            var font = Typeface.CreateFromAsset(Application.Context.Assets, "PT_Sans-Web-Bold.ttf");
            phoneTextView.Typeface = font;
            try
            {
                //string phone = Intent.Extras.GetString("phone");
                //phoneTextView.Text = phone;

            }
            catch (Exception)
            {
            }

            var dbFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var dbName = "context_db.db";
            var db = new CommentsDb(Path.Combine(dbFolderPath,dbName));
            var problemPhones = db.GetPhones().ToList();
            var listPhonesView = view.FindViewById<ListView>(Resource.Id.problemPhonesList);
            var adapter = new ProblemPhonesAdapter(Application.Context, Resource.Layout.itemProblemPhone, problemPhones);
            listPhonesView.Adapter = adapter;
            listPhonesView.ItemClick += delegate(object sender, AdapterView.ItemClickEventArgs args)
            {
                var choice = args.Position;
                Phones check = problemPhones.ElementAt(choice);
                var intent = new Intent(Activity, typeof(PhoneInfoActivity));
                intent.PutExtra("phone", check.Number);
                StartActivity(intent);
            };
            addCommentButton.Click += delegate
            {
                Activity.StartActivity(typeof(NewCommentActivity));
            };
            return view;
        }
    }
}