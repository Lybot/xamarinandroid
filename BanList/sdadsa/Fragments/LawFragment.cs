﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using delrep.Models;
using delrep.Models.Registration;
using Newtonsoft.Json;

namespace delrep.Fragments
{
    public class LawFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            var view= inflater.Inflate(Resource.Layout.LawLayout, container, false);
            var fontBold = Typeface.CreateFromAsset(Application.Context.Assets, "PT_Sans-Web-Bold.ttf");
            var font = Typeface.CreateFromAsset(Application.Context.Assets, "PT_Sans-Web-Regular.ttf");
            var headTextView = view.FindViewById<TextView>(Resource.Id.headTextView);
            var infoTextView = view.FindViewById<TextView>(Resource.Id.infoTextView);
            var getPreferences = Application.Context.GetSharedPreferences("token", FileCreationMode.Private);
            var infoLawTextView = view.FindViewById<TextView>(Resource.Id.infoLawTextView);
            infoLawTextView.Typeface = font;
            var sendApplicationButton = view.FindViewById<Button>(Resource.Id.sendApplicationButton);
            sendApplicationButton.Enabled = false;
            headTextView.Typeface = fontBold;
            infoTextView.Typeface = font;
            try
            {
                if (getPreferences.GetBoolean("getApplication", true))
                {
                    sendApplicationButton.Enabled = true;
                }
                else
                {
                    sendApplicationButton.Append(" (заявка уже была отправлена)");
                }
            }
            catch (Exception)
            {
                sendApplicationButton.Enabled = true;
            }

            sendApplicationButton.Click += async delegate
            {
                sendApplicationButton.Enabled = false;
                try
                {
                    var client = new HttpClient();
                    var commentRequest = new HttpRequestMessage(HttpMethod.Get,
                        "https://workersprojectapi.azurewebsites.net/getapplication");
                    var token = getPreferences.GetString("token", "");
                    //var myNumber = getPreferences.GetString("myNumber", "");
                    commentRequest.Headers.Add("Accept", "application/json");
                    commentRequest.Headers.Add("Authorization", "Bearer " + token);
                    HttpResponseMessage response = await client.SendAsync(commentRequest);
                    var responseContent = await response.Content.ReadAsStringAsync();
                    var responseObject = JsonConvert.DeserializeObject<ServerResponse>(responseContent);
                    if (responseObject.Success)
                    {
                        var editPreferences = getPreferences.Edit();
                        editPreferences.PutBoolean("getApplication", false);
                        editPreferences.Commit();
                        Toast.MakeText(Activity, responseObject.Text,
                            ToastLength.Long).Show();
                    }
                    else
                    {
                        Toast.MakeText(Activity, responseObject.Text,
                            ToastLength.Long).Show();
                    }
                }
                catch (Exception)
                {
                    Toast.MakeText(Application.Context, "Ошибка при отправке заявки",
                        ToastLength.Long).Show();
                }

            };
            return view;
        }
    }
}