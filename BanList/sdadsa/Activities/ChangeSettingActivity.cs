﻿using System;
using System.Net.Http;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using delrep.Models;
using delrep.Models.Registration;
using Newtonsoft.Json;

namespace delrep.Activities
{
    [Activity(Label = "ChangeSettingActivity")]
    public class ChangeSettingActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MakeSettingLayout);
            var settingBackButton = FindViewById<Button>(Resource.Id.settingBackButton);
            var settingsTextView = FindViewById<TextView>(Resource.Id.settingsTextView);
            var nameEditText = FindViewById<EditText>(Resource.Id.nameEditText);
            var firstNameEditText = FindViewById<EditText>(Resource.Id.firstNameEditText);
            var regionEditText = FindViewById<EditText>(Resource.Id.regionEditText);
            var sendButton = FindViewById<Button>(Resource.Id.sendSettingsButton);
            var fontBold = Typeface.CreateFromAsset(Assets, "PT_Sans-Web-Bold.ttf");
            var getPreferences = GetSharedPreferences("token", FileCreationMode.Private);
            try
            {
                nameEditText.Text = getPreferences.GetString("myName", "");
                firstNameEditText.Text = getPreferences.GetString("myFirstName", "");
                regionEditText.Text = getPreferences.GetString("myRegion", "");
            }
            catch (Exception)
            {
                // ignored
            }

            settingsTextView.Typeface = fontBold;
            settingBackButton.Click += delegate
            {
                Finish();
            };
            sendButton.Click += async delegate
            {
                sendButton.Enabled = false;
                var client = new HttpClient();
                var commentRequest = new HttpRequestMessage(HttpMethod.Post,
                    "https://workersprojectapi.azurewebsites.net/changeprofile");
                var token = getPreferences.GetString("token", "");
                //var myNumber = getPreferences.GetString("myNumber", "");
                commentRequest.Headers.Add("Accept", "application/json");
                commentRequest.Headers.Add("Authorization", "Bearer " + token);
                string commentString = JsonConvert.SerializeObject(new User(){Name= nameEditText.Text+" "+firstNameEditText.Text,Region = regionEditText.Text});
                commentRequest.Content = new StringContent(commentString, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.SendAsync(commentRequest);
                var responseContent = await response.Content.ReadAsStringAsync();
                var responseObject = JsonConvert.DeserializeObject<ServerResponse>(responseContent);
                var alert = new AlertDialog.Builder(this);
                if (responseObject.Success)
                {
                    alert.SetMessage(responseObject.Text);
                    alert.SetTitle("Профиль изменен");
                    alert.SetPositiveButton("Ок", (senderAlert, args) =>
                    {
                        Finish();
                    });
                    Dialog dialog = alert.Create();
                    dialog.Show();
                    var editPreferences = getPreferences.Edit();
                    editPreferences.PutString("myName", nameEditText.Text);
                    editPreferences.PutString("myFirstName", firstNameEditText.Text);
                    editPreferences.PutString("myRegion", regionEditText.Text);
                    editPreferences.Commit();
                    sendButton.Enabled = true;
                }
            };

            // Create your application here
        }
    }
}