﻿using System;
using System.Net.Http;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using delrep.Models;
using delrep.Models.DatabaseModels;
using delrep.Models.Registration;
using Environment = System.Environment;
using Path = System.IO.Path;

namespace delrep.Activities
{
    [Activity(Label = "newCommentActivity")]
    public class NewCommentActivity : Activity
    {
        private EditText _numberEditText;
        private EditText _firstNameEditText;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            var fontBold = Typeface.CreateFromAsset(Assets, "PT_Sans-Web-Bold.ttf");
            var font = Typeface.CreateFromAsset(Assets, "PT_Sans-Web-Regular.ttf");
            var sendView = FindViewById<Button>(Resource.Id.sendComment);
            var nameActivity = FindViewById<TextView>(Resource.Id.ActivityName);
            var whosCommentTextView = FindViewById<TextView>(Resource.Id.whosCommentTextView);
            _numberEditText = FindViewById<EditText>(Resource.Id.numberEditText);
            _firstNameEditText = FindViewById<EditText>(Resource.Id.firstNameEditText);
            var nameEditText = FindViewById<EditText>(Resource.Id.nameEditText);
            var essenceCommentTextView = FindViewById<TextView>(Resource.Id.essenceCommentTextView);
            var commentBox = FindViewById<EditText>(Resource.Id.commentBox);
            var debtEditText = FindViewById<EditText>(Resource.Id.debtEditText);
            var lawerViewButton = FindViewById<Button>(Resource.Id.lawerViewButton);
            var closeImageButton = FindViewById<ImageButton>(Resource.Id.closeImageButton);
            var fullNameLinearLayout = FindViewById<LinearLayout>(Resource.Id.fullNameLinearLayout);
            var confirmLawTextView = FindViewById<TextView>(Resource.Id.confirmLawTextView);
            //var findNumberButton = FindViewById<Button>(Resource.Id.findNumberButton);
            whosCommentTextView.Typeface = font;
            _firstNameEditText.Typeface = font;
            nameEditText.Typeface = font;
            essenceCommentTextView.Typeface = font;
            commentBox.Typeface = font;
            debtEditText.Typeface = font;
            confirmLawTextView.Typeface = font;
            _numberEditText.Typeface = font;
            nameActivity.Typeface=fontBold;
            lawerViewButton.Click += delegate { Toast.MakeText(this, "В следующих обновлениях", ToastLength.Long); };
            closeImageButton.Click += delegate
            {
                Finish();
            };
            //findNumberButton.Click += delegate
            //{
            //    var intent = new Intent(this,typeof(ChoicePhone));
            //    StartActivityForResult(intent,228);
            //};
            _numberEditText.TextChanged += delegate
            {
                if (_numberEditText.Text.Length == 10)
                {
                    fullNameLinearLayout.Visibility = ViewStates.Visible;
                    _firstNameEditText.RequestFocus();
                }

                if (_numberEditText.Text.Length > 10)
                {
                    string parsedText = _numberEditText.Text.Replace(" ","");
                    parsedText = parsedText.Substring(parsedText.Length-10, 10);
                    _numberEditText.Text = parsedText;
                    _numberEditText.SetSelection(_numberEditText.Text.Length);
                }

                //if (numberEditText.Text.First() == '+'||numberEditText.Text.First()=='8')
                //{
                //    numberEditText.Text = ""+numberEditText.Text.Substring(1);
                //}
            };
            sendView.Click += async delegate
            {
                var mDialog = new AlertDialog.Builder(this);
                var dialogWindow = mDialog.Create();
                View view = View.Inflate(this, Resource.Layout.ProgressDialogLayout, null);
                view.FindViewById<TextView>(Resource.Id.textView).Text = "Отправка претензии...";
                dialogWindow.SetView(view);
                dialogWindow.Window.AddFlags(WindowManagerFlags.NotTouchable
                );
                try
                {
                    sendView.Enabled = false;
                    int debt;
                    try
                    {
                        debt = Convert.ToInt32(debtEditText.Text);
                    }
                    catch (Exception)
                    {
                        Toast.MakeText(this, "Введены некорректные данные", ToastLength.Short).Show();
                        sendView.Enabled = true;
                        
                        throw;
                    }
                    if (_numberEditText.Text.Length > 12 || commentBox.Text == ""||_numberEditText.Text.Length<10)
                    {
                        Toast.MakeText(ApplicationContext, "Введены некорректные данные", ToastLength.Short).Show();
                        sendView.Enabled = true;
                        return;
                    }
                    dialogWindow.Show();
                    var name = _firstNameEditText.Text.Trim() + " " + nameEditText.Text.Trim();
                    var client = new HttpClient();
                    var commentRequest = new HttpRequestMessage(HttpMethod.Post,
                        "https://workersprojectapi.azurewebsites.net/makecomment");
                    var getPreferences = GetSharedPreferences("token", FileCreationMode.Private);
                    var token = getPreferences.GetString("token", "");
                    //var myNumber = getPreferences.GetString("myNumber", "");
                    commentRequest.Headers.Add("Accept", "application/json");
                    commentRequest.Headers.Add("Authorization", "Bearer " + token);
                    string parsedNumber = _numberEditText.Text;
                    if (_numberEditText.Text.Length > 10)
                        parsedNumber = _numberEditText.Text.Substring(_numberEditText.Text.Length - 10, 10);
                    string commentString = JsonConvert.SerializeObject(new CommentClient(commentBox.Text, parsedNumber, debt, DateTime.Today.ToShortDateString(),name));
                    commentRequest.Content = new StringContent(commentString, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.SendAsync(commentRequest);
                    var responseContent = await response.Content.ReadAsStringAsync();
                    var responseObject = JsonConvert.DeserializeObject<ServerResponse>(responseContent);
                    var alert = new AlertDialog.Builder(this);
                    if (responseObject.Success)
                    {
                        dialogWindow.Dismiss();
                        alert.SetMessage(responseObject.Text);
                        alert.SetTitle("Комментарий отправлен");
                        
                        alert.SetPositiveButton("Ок", (senderAlert, args) =>
                        {
                            sendView.Enabled = true;
                            Finish();
                        });
                        Dialog dialog = alert.Create();
                        dialog.Show();
                        var dbFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                        var dbName = "context_db.db";
                        var db = new CommentsDb(Path.Combine(dbFolderPath, dbName));
                         await  db.UpdateDb(Path.Combine(dbFolderPath, dbName), token);
                    }
                    else
                    {
                        dialogWindow.Dismiss();
                        alert.SetMessage(responseObject.Text);
                        alert.SetTitle("Ошибка");
                        alert.SetPositiveButton("Ок", (senderAlert, args) =>
                        {
                        });
                        Dialog dialog = alert.Create();
                        dialog.Show();
                        sendView.Enabled = true;
                    }
                }
                catch (Exception)
                {
                    Toast lostInternet = Toast.MakeText(Application.Context, "Произошла ошибка", ToastLength.Short);
                    lostInternet.Show();
                    dialogWindow.Dismiss();
                    sendView.Enabled = true;
                }
            };
            //change.Click += delegate
            //{
            //    Intent choicePhone = new Intent(this, typeof(ChoicePhone));
            //    StartActivity(choicePhone);
            //};
            try
            {
                var phone = Intent.Extras.GetString("selectedPhone");
                _numberEditText.Append(phone);
            }
            catch (Exception)
            {
                // ignored
            }
        }
        // Create your application here
        //protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        //{
        //    try
        //    {
        //        if (requestCode == 228)
        //        {
        //            _numberEditText.Text = data.GetStringExtra("selectedPhone");
        //            if (data.GetStringExtra("selectedName") != "Без имени")
        //                _firstNameEditText.Text = data.GetStringExtra("selectedName");
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        //ignore
        //    }
        //    base.OnActivityResult(requestCode, resultCode, data);

        //}
    }
    }
    
