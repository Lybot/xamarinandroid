﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Provider;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using delrep.Fragments;
using delrep.Models;
using delrep.Models.DatabaseModels;
using Newtonsoft.Json;
using AlertDialog = Android.App.AlertDialog;
using Environment = System.Environment;
using Exception = System.Exception;
using Uri = Android.Net.Uri;

namespace delrep.Activities
{
    [Activity(Label = "NavigationActivity", Theme = "@style/AppTheme")]
    public class NavigationActivity : AppCompatActivity, BottomNavigationView.IOnNavigationItemSelectedListener
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var mDialog = new AlertDialog.Builder(this);
            var dialogWindow = mDialog.Create();
            View view = View.Inflate(this, Resource.Layout.ProgressDialogLayout, null);
            view.FindViewById<TextView>(Resource.Id.textView).Text = "Загрузка...";
            view.FindViewById(Resource.Id.progressBar);
            dialogWindow.SetView(view);
            dialogWindow.Window.AddFlags(WindowManagerFlags.NotTouchable
            );
            dialogWindow.Show();
            var dbFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var dbName = "context_db.db";
            new Thread(new ThreadStart(async delegate
            {
                bool success;             
                try
                {
                    var preferences = GetSharedPreferences("token", FileCreationMode.Private);
                    var token = preferences.GetString("token", "");
                    var preferencesEdit = preferences.Edit();
                    preferencesEdit.PutString("dbpath", Path.Combine(dbFolderPath, dbName));
                    if (token == "")
                    {
                        Toast.MakeText(this, "Нужна повторная авторизация", ToastLength.Long);
                        StartActivity(typeof(LoginActivity));
                    }
                    var db = new CommentsDb(Path.Combine(dbFolderPath, dbName));
                    await db.FullUpdateDb(Path.Combine(dbFolderPath, dbName), token);
                    var client = new HttpClient();
                    var identityRequest = new HttpRequestMessage(HttpMethod.Get,
                        "https://workersprojectapi.azurewebsites.net/getidentity");
                    identityRequest.Headers.Add("Accept", "application/json");
                    identityRequest.Headers.Add("Authorization", "Bearer " + token);
                    var response = await client.SendAsync(identityRequest);
                    var responseString = await response.Content.ReadAsStringAsync();
                    var responseObject = JsonConvert.DeserializeObject<User>(responseString);
                    preferencesEdit.PutString("myName", responseObject.Name);
                    preferencesEdit.PutString("myFirstName", responseObject.FirstName);
                    preferencesEdit.PutString("myRegion", responseObject.Region);
                    preferencesEdit.PutBoolean("premium", responseObject.Premium);
                    preferencesEdit.Commit();
                    success = true;
                }
                catch (Exception)
                {
                    success = false;
                }
                RunOnUiThread(() =>
                {
                    if (!success)
                        Toast.MakeText(this, "Произошла ошибка при обновлении базы", ToastLength.Long);
                    else
                    {
                        FragmentTransaction ft = FragmentManager.BeginTransaction();
                        PhoneInfoFragment phoneInfoFragment = new PhoneInfoFragment();
                        try
                        {
                            var preferences = GetSharedPreferences("token", FileCreationMode.Private);
                            var name = preferences.GetString("myName", "");
                            if (String.IsNullOrWhiteSpace(name))
                            {
                                var builder = new AlertDialog.Builder(this);
                                builder.SetPositiveButton("Изменить",
                                    delegate { StartActivity(typeof(ChangeSettingActivity)); });
                                builder.SetNegativeButton("Пропустить", delegate { });
                                builder.SetTitle("Настройка профиля");
                                builder.SetMessage(
                                    "Ваш профиль не настроен. Ваше имя отображается в оставленных вами претензиях.");
                                var alert = builder.Create();
                                alert.Show();
                            }
                        }
                        catch (Exception)
                        {
                            //ignored
                        }
                        ft.Replace(Resource.Id.frameLayout, phoneInfoFragment);
                        ft.Commit();
                        dialogWindow.Dismiss();
                        try
                        {
                            var preferences = GetSharedPreferences("token", FileCreationMode.Private);
                            if ((int)Build.VERSION.SdkInt >= 23)
                            if (!preferences.GetBoolean("allowOverlay", false))
                            {
                                var builder = new AlertDialog.Builder(this);
                                builder.SetPositiveButton("Изменить",
                                    delegate
                                    {
                                        Intent intent = new Intent(Settings.ActionManageOverlayPermission,
                                            Uri.Parse("package:" + PackageName));
                                        StartActivityForResult(intent, 322);
                                        var edit = preferences.Edit();
                                        edit.PutBoolean("allowOverlay", true);
                                        edit.Apply();
                                    });
                                builder.SetTitle("Необходимо разрешение");
                                builder.SetMessage(
                                    "Для отображения проблемного клиента необходимо разрешение на наложение поверх других окон");
                                var alert = builder.Create();
                                alert.Show();
                            }
                        }
                        catch (Exception)
                        {
                            //check
                        }
                    }
                });
            })).Start(); 
            SetContentView(Resource.Layout.NavigationLayout);
            BottomNavigationView navigation = FindViewById<BottomNavigationView>(Resource.Id.bottomNavigationView);
            navigation.SetOnNavigationItemSelectedListener(this);
            navigation.Menu.GetItem(2).SetChecked(true);
            string[] needPermissions = { Manifest.Permission.SystemAlertWindow, Manifest.Permission.ReadPhoneState,  Manifest.Permission.InternalSystemWindow };
            try
            {
                if (!((int)Build.VERSION.SdkInt < 23 || CheckSelfPermission(Manifest.Permission.ReadPhoneState) == Permission.Granted || 
                      CheckSelfPermission(Manifest.Permission.SystemAlertWindow)==Permission.Granted))
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.SetTitle("Выполните настройки");
                    alert.SetMessage("Для определения входящего номера и выдачи уведомлений требуется предоставить следующее разрешение");
                    alert.SetPositiveButton("Далее", (senderAlert, args) =>
                    {
                        ComponentName windowReceiver = new ComponentName(this, "com.app.IncomingCallReceiver");
                        ComponentName notificationReceiver = new ComponentName(this, "com.app.IncomingCallReceiverNotification");
                        PackageManager pm = PackageManager;
                        pm.SetComponentEnabledSetting(notificationReceiver, ComponentEnabledState.Enabled, ComponentEnableOption.DontKillApp);
                        pm.SetComponentEnabledSetting(windowReceiver,ComponentEnabledState.Enabled,ComponentEnableOption.DontKillApp);
                        RequestPermissions(needPermissions, 0);
                    });
                    Dialog dialog = alert.Create();
                    dialog.Show();
                }
            }
            catch (Exception)
            {
                var toast = new Toast(ApplicationContext);
                toast.SetText("Произошли ошибки при запросе разрешения");
                toast.Show();
            }
        }
        public bool OnNavigationItemSelected(IMenuItem item)
        {
            FragmentTransaction ft = FragmentManager.BeginTransaction();
            //NewCommentFragment newComment = new NewCommentFragment();
            //ft.Replace(Resource.Id.frameLayout, newComment);
            //ft.Commit();
            switch (item.ItemId)
            {
                case Resource.Id.navigation_search:
                    SearchFragment search = new SearchFragment();
                    ft.Replace(Resource.Id.frameLayout, search);
                    ft.Commit();
                    return true;
                case Resource.Id.navigation_home:
                    LawFragment newComment = new LawFragment();
                    ft.Replace(Resource.Id.frameLayout, newComment);
                    ft.Commit();
                    return true;
                case Resource.Id.navigation_dashboard:
                    //textMessage.SetText(Resource.String.title_dashboard);
                    PhoneInfoFragment phoneInfo = new PhoneInfoFragment();
                    // The fragment will have the ID of Resource.Id.fragment_container.
                    ft.Replace(Resource.Id.frameLayout, phoneInfo);
                    ft.Commit();
                    return true;
                case Resource.Id.navigation_notifications:
                    SettingsFragment settings = new SettingsFragment();
                    ft.Replace(Resource.Id.frameLayout, settings);
                    ft.Commit();
                    return true;
            }
            return false;

        }

        public override void OnBackPressed()
        {
           
        }
    }
}