﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using delrep;
using delrep.Models;
using delrep.Models.DatabaseModels;
using delrep.Models.PhoneInfoList;
using Environment = System.Environment;

namespace delrep.Activities
{
    [Activity(Label = "PhoneInfo", Theme = "@style/AppTheme")]
    public class PhoneInfoActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            string phone = Intent.Extras.GetString("phone");
            var mDialog = new AlertDialog.Builder(this);
            var dialogWindow = mDialog.Create();
            View view = View.Inflate(this, Resource.Layout.ProgressDialogLayout, null);
            view.FindViewById<TextView>(Resource.Id.textView).Text = "Загрузка...";
            dialogWindow.SetView(view);
            dialogWindow.Window.AddFlags(WindowManagerFlags.NotTouchable
            );
            dialogWindow.Show();
            new Thread(new ThreadStart(async delegate
            {
                bool success;
                List<CommentClient> commentsList = new List<CommentClient>() { new CommentClient("Проблемы с интернетом", "или необходима повторная авторизация в приложении", 0, DateTime.Now.ToShortDateString(), "Ошибка?") };
                try
            {
                HttpClient client = new HttpClient();
                var commentRequest = new HttpRequestMessage(HttpMethod.Post,
                    "https://workersprojectapi.azurewebsites.net/getCommentsPhone");
                var getPreferences = GetSharedPreferences("token", FileCreationMode.Private);
                var token = getPreferences.GetString("token", "");
                commentRequest.Headers.Add("Accept", "application/json");
                commentRequest.Headers.Add("Authorization", "Bearer " + token);
                string commentString = JsonConvert.SerializeObject(new Phones() {Number = phone});
                commentRequest.Content = new StringContent(commentString, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.SendAsync(commentRequest);
                var responseContent = await response.Content.ReadAsStringAsync();
                var responseObject = JsonConvert.DeserializeObject<IEnumerable<CommentClient>>(responseContent);
                commentsList = responseObject.ToList();
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }
                RunOnUiThread(() =>
                {
                    if (success)
                    {
                        dialogWindow.Dismiss();
                        SetContentView(Resource.Layout.PhoneInfoLayout);
                        var buttonFindView = FindViewById<Button>(Resource.Id.backButton);
                        var phoneTextView = FindViewById<TextView>(Resource.Id.phoneText);
                        var addCommentButton = FindViewById<Button>(Resource.Id.addCommentButton);
                        phoneTextView.Text = FormatPhone.FormatNumber(phone);
                        addCommentButton.Click += delegate
                        {
                            var intent = new Intent(this, typeof(NewCommentActivity));
                            intent.PutExtra("selectedPhone", phone);
                            Finish();
                            StartActivity(intent);
                        };
                        var adapter = new PhoneInfoAdapter(this, Resource.Layout.Item_phoneInfo, commentsList);
                        var listCommentsView = FindViewById<ListView>(Resource.Id.commentsListView);
                        listCommentsView.Adapter = adapter;
                        success = true;
                        buttonFindView.Click += delegate { this.FinishAndRemoveTask(); };
                    }
                    else
                    {
                        dialogWindow.Dismiss();
                        Toast.MakeText(this,
                            "Отстутствует соединение с интернетом или произошла ошибка", ToastLength.Long).Show();
                    }
                    
                });
            })).Start();
        }
    }
}