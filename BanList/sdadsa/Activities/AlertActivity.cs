﻿
using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using delrep;

namespace delrep.Activities
{
    [Activity(Label = "AlertActivity",Theme = "@style/AlertTheme",WindowSoftInputMode = SoftInput.StateAlwaysVisible)]
    public class AlertActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            //var manager =(IWindowManager) ApplicationContext.GetSystemService(WindowService);
            try
            {
                View view = View.Inflate(this, Resource.Layout.alertLayout, null);
                WindowManagerLayoutParams param;
                if((int)Build.VERSION.SdkInt > 25)
                   param = new WindowManagerLayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent, WindowManagerTypes.ApplicationOverlay,
                    WindowManagerFlags.NotFocusable, Format.Translucent);
                else
                    param = new WindowManagerLayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent, WindowManagerTypes.SystemAlert,
                        WindowManagerFlags.NotFocusable, Format.Translucent);
                param.Gravity = GravityFlags.Center;
                var okButton = view.FindViewById<Button>(Resource.Id.alertOkButton);
                var infoButton = view.FindViewById<Button>(Resource.Id.alertInfoButton);
                okButton.Click += delegate
                {
                    WindowManager.RemoveView(view);
                    base.Finish();
                };
                infoButton.Click += delegate
                {
                    WindowManager.RemoveView(view);
                    Intent newActivity = new Intent(this, typeof(PhoneInfoActivity));
                    newActivity.PutExtra("phone", Intent.Extras.GetString("phone"));
                    StartActivity(newActivity);
                    Finish();
                };
                WindowManager.AddView(view, param);
               }
            catch (Exception)
            {
                var builder = new AlertDialog.Builder(this);
                var alert = builder.Create();
                View view = View.Inflate(this, Resource.Layout.alertLayout, null);
                var okButton = view.FindViewById<Button>(Resource.Id.alertOkButton);
                var infoButton = view.FindViewById<Button>(Resource.Id.alertInfoButton);
                okButton.Click += delegate
                {
                    alert.Dismiss();
                    base.Finish();
                };
                infoButton.Click += delegate
                {
                    
                    Intent newActivity = new Intent(this, typeof(PhoneInfoActivity));
                    newActivity.PutExtra("phone", Intent.Extras.GetString("phone"));
                    StartActivity(newActivity);
                    alert.Dismiss();
                    Finish();
                };
                alert.SetView(view);
                alert.Show();
            }

            //manager.AddView(view,kek);
        }
    }
}