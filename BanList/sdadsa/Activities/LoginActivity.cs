﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Newtonsoft.Json;
using delrep.Models.Registration;

namespace delrep.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class LoginActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            HttpClient client = new HttpClient();
            string token = String.Empty;
            try
            {
                var getPreferences = GetSharedPreferences("token", FileCreationMode.Private);
                token = getPreferences.GetString("token", "");
            }
            catch (Exception)
            {
                // ignored
            }

            if (!String.IsNullOrEmpty(token))
            {
                var mDialog = new AlertDialog.Builder(this);
                var dialogWindow = mDialog.Create();
                View view = View.Inflate(this, Resource.Layout.ProgressDialogLayout, null);
                view.FindViewById<TextView>(Resource.Id.textView).Text = "Вход...";
                dialogWindow.SetView(view);
                dialogWindow.Window.AddFlags(WindowManagerFlags.NotTouchable
                );
                dialogWindow.Show();
                new Thread(new ThreadStart(async delegate
                {
                    bool success = false;
                    try
                    {
                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get,
                            "https://workersprojectapi.azurewebsites.net/tryconnect");
                        request.Headers.Add("Accept", "application/json");
                        request.Headers.Add("Authorization", "Bearer " + token);
                        var response = await client.SendAsync(request);
                        if (response.IsSuccessStatusCode)
                        {
                            success = true;
                            StartActivity(typeof(NavigationActivity));
                            base.Finish();
                        }
                    }
                    catch (Exception)
                    {
                        success = false;
                    }

                    RunOnUiThread(() =>
                    {
                        if (success)
                            dialogWindow.Dismiss();
                        else
                        {
                            Toast.MakeText(this, "Необходима повторная авторизация", ToastLength.Long).Show();
                            dialogWindow.Dismiss();
                        }
                    });
                })).Start();

            }

            SetContentView(Resource.Layout.LoginLayout);
            var authButton = FindViewById<Button>(Resource.Id.AuthButton);
            var loginView = FindViewById<EditText>(Resource.Id.LoginEditText);
            var font = Typeface.CreateFromAsset(Assets, "PT_Sans-Web-Bold.ttf");
            var authTextView = FindViewById<TextView>(Resource.Id.AuthTextView);
            authTextView.Typeface = font;
            loginView.Click += delegate
            {
                if (loginView.Text == "")
                {
                    loginView.Append("+7 ");
                }
            };
            loginView.PerformClick();
            loginView.TextChanged += delegate
            {
                if (loginView.Text.Length == 6 || loginView.Text.Length == 10)
                {
                    loginView.Append(" ");
                }

                if (loginView.Text.Length == 15)
                {
                    loginView.ClearFocus();
                    authButton.RequestFocus();
                    InputMethodManager inputManager = (InputMethodManager)GetSystemService(InputMethodService);
                    inputManager.HideSoftInputFromWindow(CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
                }

                if (loginView.Text.Length > 15)
                {
                    loginView.Text = loginView.Text.Substring(0, 15);
                    loginView.SetSelection(15);
                }
            };

        authButton.Click += async delegate
            {
                authButton.Enabled = false;
                var mDialog = new AlertDialog.Builder(this);
                var dialogWindow = mDialog.Create();
                View view = View.Inflate(this, Resource.Layout.ProgressDialogLayout, null);
                view.FindViewById<TextView>(Resource.Id.textView).Text = "Отправка смс...";
                dialogWindow.SetView(view);
                dialogWindow.Window.AddFlags(WindowManagerFlags.NotTouchable
                );
                try
                {
                    authButton.Enabled = false;
                    string spacelessPhone = loginView.Text.Replace(" ", "");
                    if (spacelessPhone.Length < 11)
                    {
                        Toast.MakeText(this, "Некорректный номер телефона", ToastLength.Long);
                        return;
                    }
                    string parsedPhone = spacelessPhone.Substring(spacelessPhone.Length - 10, 10);
                    dialogWindow.Show();
                    //TODO 
                    HttpRequestMessage authMessage = new HttpRequestMessage(HttpMethod.Post,
                           "https://workersprojectapi.azurewebsites.net/register");
                    ServerRequest request = new ServerRequest()
                    {
                        Phone = parsedPhone
                    };
                    authMessage.Content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8,
                        "application/json");
                    authMessage.Headers.Add("Accept", "application/json");
                    var response = await client.SendAsync(authMessage);
                    var responseString = await response.Content.ReadAsStringAsync();
                    var responseObject = JsonConvert.DeserializeObject<ServerResponse>(responseString);
                    if (responseObject.Success == false)
                    {
                        Toast.MakeText(this,responseObject.Text,ToastLength.Long).Show();
                        dialogWindow.Dismiss();
                    }
                    else
                    {
                        var tokenPreferences = GetSharedPreferences("token", FileCreationMode.Private);
                        ISharedPreferencesEditor settings = tokenPreferences.Edit();
                        settings.PutString("myNumber", parsedPhone);
                        settings.Apply();
                        authButton.Enabled = true;
                        dialogWindow.Dismiss();
                        StartActivity(typeof(ConfirmActivity));
                        base.Finish();
                    }
                }
                catch (Exception)
                {
                    var toast = Toast.MakeText(this, "Отстуствует соединение с интернетом", ToastLength.Short);
                    toast.Show();
                    dialogWindow.Dismiss();
                    authButton.Enabled  = true;
                }
                };
            }
    }
}