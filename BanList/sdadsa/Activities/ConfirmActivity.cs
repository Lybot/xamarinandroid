﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using delrep.Models;
using Newtonsoft.Json;
using delrep.Models.Registration;

namespace delrep.Activities
{
    [Activity(Label = "ConfirmActivity")]
    public class ConfirmActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.confirmLayout);
            var fontBold = Typeface.CreateFromAsset(Assets, "PT_Sans-Web-Bold.ttf");
            var fontRegular = Typeface.CreateFromAsset(Assets, "PT_Sans-Web-Regular.ttf");
            var authDataTextView = FindViewById<TextView>(Resource.Id.authDataTextView);
            var confirmTextTextView = FindViewById<TextView>(Resource.Id.confirmTextTextView);
            var enterCodeTextView = FindViewById<TextView>(Resource.Id.enterCodeTextView);
            var timeTextView = FindViewById<TextView>(Resource.Id.timeTextView);
            var codeEditText = FindViewById<EditText>(Resource.Id.codeEditText);
            var backTextView = FindViewById<TextView>(Resource.Id.backTextView);
            var authButton = FindViewById<Button>(Resource.Id.authButton);
            var preferences = GetSharedPreferences("token", FileCreationMode.Private);
            var phone= preferences.GetString("myNumber", "");
            codeEditText.Typeface = fontRegular;
            confirmTextTextView.Append(" "+FormatPhone.FormatNumber(phone));
            authDataTextView.Typeface = fontBold;
            enterCodeTextView.Typeface = fontRegular;
            confirmTextTextView.Typeface = fontRegular;
            timeTextView.Typeface = fontRegular;
            HttpClient client = new HttpClient();
            backTextView.Click += delegate
            {
                Finish();
                StartActivity(typeof(LoginActivity));
            };
            codeEditText.TextChanged += delegate
            {
                if (codeEditText.Text.Length == 4)
                {
                    InputMethodManager inputManager =
                        (InputMethodManager) this.GetSystemService(Context.InputMethodService);
                    inputManager.HideSoftInputFromWindow(this.CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
                }
            };
            authButton.Click += delegate
            {
                try
                {
                    if (codeEditText.Text.Length != 4)
                    {
                        Toast.MakeText(this, "Введен некорректный код", ToastLength.Long);
                    }
                    else
                    {
                        //TODO 
                        HttpRequestMessage authMessage = new HttpRequestMessage(HttpMethod.Post,
                            "https://workersprojectapi.azurewebsites.net/api/auth/auth");
                        ServerRequest request = new ServerRequest()
                        {
                            Sms = codeEditText.Text,
                            Phone = phone
                        };
                        authMessage.Content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8,
                            "application/json");
                        authMessage.Headers.Add("Accept", "application/json");
                        var response = client.SendAsync(authMessage).GetAwaiter().GetResult();
                        var responseString = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                        var responseObject = JsonConvert.DeserializeObject<ServerResponse>(responseString);
                        if (responseObject.Success == false)
                        {
                            Toast.MakeText(this, responseObject.Text, ToastLength.Long).Show();
                        }
                        else
                        {
                            authButton.Clickable = true;
                            var tokenPreferences = GetSharedPreferences("token", FileCreationMode.Private);
                            ISharedPreferencesEditor settings = tokenPreferences.Edit();
                            settings.PutString("token", responseObject.Token);
                            settings.Apply();
                            StartActivity(typeof(NavigationActivity));
                            base.Finish();
                        }
                    }
                }
                catch (Exception)
                {
                    var toast = Toast.MakeText(this, "Отстуствует соединение с интернетом", ToastLength.Short);
                    toast.Show();
                    authButton.Clickable = true;
                }
            };
        }
        }
    }
