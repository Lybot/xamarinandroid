﻿using System.Collections.Generic;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using delrep.Models;
using delrep.Models.PhoneInfoList;

namespace delrep.Activities
{
    [Activity(Label = "ListActivity", Theme = "@style/AppTheme")]
    public class ChoicePhone : ListActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            if (((int) Build.VERSION.SdkInt < 23) ||
                (CheckSelfPermission(Manifest.Permission.ReadCallLog) == Permission.Granted))
            {
                var phones = GetCallLog.GetCallingNumbers();
                ListAdapter = new FindPhoneAdapter(this, Resource.Layout.item_find_phone_layout, phones);
            }
            else
            {
                string[] needPermissions = {Manifest.Permission.ReadPhoneState, Manifest.Permission.ReadCallLog};
                RequestPermissions(needPermissions, 0);

                if (CheckSelfPermission(Manifest.Permission.ReadCallLog) == Permission.Granted)
                {
                    var phones = GetCallLog.GetCallingNumbers();
                    ListAdapter = new FindPhoneAdapter(this, Resource.Layout.item_find_phone_layout, phones);
                }
                else
                    ListAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1,
                        new List<string>() {"Для выбора телефона требуется разрешение", "На чтение истории звонков (телефон)"});

            }

            ListView.TextFilterEnabled = true;
            ListView.ItemClick += delegate(object sender, AdapterView.ItemClickEventArgs args)
            {
                var view= args.View;
                view.FindViewById(Resource.Id.phoneTextView);
                string phone = view.FindViewById<TextView>(Resource.Id.phoneTextView).Text;
                if (phone.Length > 10)
                    phone = phone.Substring(phone.Length - 10, 10);
                string name = view.FindViewById<TextView>(Resource.Id.nameTextView).Text;
                
                var intent = new Intent();
                intent.PutExtra("selectedPhone", phone);
                intent.PutExtra("selectedName", name);
                SetResult(Result.Ok,intent);
                Finish();
            };
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions,
            Permission[] grantResults)
        {
            if (grantResults[0] == Permission.Granted)
            {
                var phones = GetCallLog.GetCallingNumbers();
                ListAdapter = new FindPhoneAdapter(this, Resource.Layout.item_find_phone_layout, phones);
            }
            else
            {
                ListAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1,
                    new List<string>() {"Для выбора телефона требуется разрешение", "На чтение истории звонков"});
            }
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}