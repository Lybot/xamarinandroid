﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Telephony;
using Android.Views;
using Android.Widget;
using delrep.Models.DatabaseModels;

namespace delrep.Models
{
    public class GetCallLog
    {
        public static List<Phones> GetCallingNumbers()
        {
            var phones = new List<Phones>{};
            var numbers = new List<string> { };
            Context myContext = Application.Context;
            ICursor cursor = myContext.ContentResolver.Query(Android.Net.Uri.Parse("content://call_log/calls"), null,
                null, null, null);
            if (cursor.MoveToFirst())
            {
                while (!cursor.IsAfterLast)
                {
                    string number = cursor.GetString(cursor.GetColumnIndex(CallLog.Calls.Number));
                    string name = cursor.GetString(cursor.GetColumnIndex(CallLog.Calls.CachedName));
                    if (string.IsNullOrEmpty(name))
                        name = "Без имени";
                    if (!numbers.Contains(number))
                    {
                        Phones phone = new Phones() { Name = name, Number = number };
                        numbers.Add(number);
                        phones.Add(phone);
                    }
                    cursor.MoveToNext();
                }
            }
            return phones;
        }
    }
}