﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using delrep.Models.DatabaseModels;

namespace delrep.Models.PhoneInfoList
{
    class ProblemPhonesAdapter : ArrayAdapter<Phones>
    {

        private LayoutInflater _inflater;
        private readonly int _layout;
        private List<Phones> _problemPhones;
        public ProblemPhonesAdapter(Context context, int resource, List<Phones> phones) : base(context, resource, phones)
        {
            this._problemPhones = phones;
            this._layout = resource;
            this._inflater = LayoutInflater.From(context);
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = _inflater.Inflate(this._layout, parent, false);
            TextView phoneView = (TextView)view.FindViewById<TextView>(Resource.Id.phoneTextView);
            var font = Typeface.CreateFromAsset(Context.Assets, "PT_Sans-Web-Regular.ttf");
            var fontBold = Typeface.CreateFromAsset(Context.Assets, "PT_Sans-Web-Bold.ttf");
            var nameTextView = view.FindViewById<TextView>(Resource.Id.nameTextView);
            nameTextView.Typeface = fontBold;
            phoneView.Typeface = font;
            Phones phone = _problemPhones.ElementAt(position);
            if (String.IsNullOrEmpty(phone.Name.Trim(' ')))
                phone.Name = "Без имени";
            phoneView.Text = FormatPhone.FormatNumber(phone.Number);
            nameTextView.Text = phone.Name;
            return view;
        }
    }
}