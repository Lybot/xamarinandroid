﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using delrep.Models.DatabaseModels;

namespace delrep.Models.PhoneInfoList
{
    class FindPhoneAdapter : ArrayAdapter<Phones>
    {
        private LayoutInflater _inflater;
        private readonly int _layout;
        private List<Phones> _problemPhones;

        public FindPhoneAdapter(Context context, int resource, List<Phones> phones) : base(context, resource, phones)
        {
            this._problemPhones = phones;
            this._layout = resource;
            this._inflater = LayoutInflater.From(context);
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = _inflater.Inflate(this._layout, parent, false);
            var font = Typeface.CreateFromAsset(Context.Assets, "PT_Sans-Web-Regular.ttf");
            var phoneTextView = view.FindViewById<TextView>(Resource.Id.phoneTextView);
            var nameTextView = view.FindViewById<TextView>(Resource.Id.nameTextView);
            phoneTextView.Typeface = font;
            nameTextView.Typeface = font;
            var phone = _problemPhones.ElementAt(position);
            if (String.IsNullOrWhiteSpace(phone.Name))
                phone.Name = "Без имени";
            nameTextView.Text = phone.Name;
            phoneTextView.Text = phone.Number;
            return view;
        }
    }
}