﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Uri = Android.Net.Uri;

namespace delrep.Models.PhoneInfoList
{
    public class PhoneInfoAdapter : ArrayAdapter<CommentClient>
    {
        private LayoutInflater _inflater;
        private readonly int _layout;
        private List<CommentClient> _comments;
        public PhoneInfoAdapter(Context context, int resource, List<CommentClient> comments) : base(context,resource,comments)
        {
            this._comments =comments;
            this._layout = resource;
            this._inflater = LayoutInflater.From(context);
        }
        public override View GetView(int position, View convertView, ViewGroup parent) 
        {
            View view = _inflater.Inflate(this._layout, parent, false);
            TextView commentatorView =(TextView) view.FindViewById<TextView>(Resource.Id.commentatorText);
            TextView dateView = (TextView)view.FindViewById<TextView>(Resource.Id.dateText);
            TextView commentView = (TextView)view.FindViewById<TextView>(Resource.Id.commentText);
            TextView commentatorNameTextView =
                (TextView) view.FindViewById<TextView>(Resource.Id.commentatorNameTextView);
            var nameTextView = view.FindViewById<TextView>(Resource.Id.nameTextView);
            TextView debtView = (TextView)view.FindViewById<TextView>(Resource.Id.debtText);
            var callButton = view.FindViewById<ImageButton>(Resource.Id.callImageButton);
            var font = Typeface.CreateFromAsset(Context.Assets, "PT_Sans-Web-Regular.ttf");
            dateView.Typeface = font;
            commentView.Typeface = font;
            commentatorView.Typeface = font;
            debtView.Typeface = font;
            commentatorNameTextView.Typeface = font;
            CommentClient comment = _comments.ElementAt(position);
            commentatorView.Text = FormatPhone.FormatNumber(comment.CreatorPhone);
            nameTextView.Append(" "+comment.Name);
            commentView.Text = comment.Text;
            commentatorNameTextView.Text = comment.CreatorName;
            debtView.Append(" "+comment.Debt.ToString());
            dateView.Append(": "+comment.Date);
            callButton.Click += delegate
            {
                var intent = new Intent(Intent.ActionDial, Uri.Parse("tel:" +"8"+comment.CreatorPhone));
                Context.StartActivity(intent);
            };
            return view;
        }
    }
}