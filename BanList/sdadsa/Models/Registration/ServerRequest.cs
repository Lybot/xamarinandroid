﻿namespace delrep.Models.Registration
{
    public class ServerRequest
    {
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Sms { get; set; }
        public string Region { get; set; }
    }
}