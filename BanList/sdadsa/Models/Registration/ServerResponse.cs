﻿namespace delrep.Models.Registration
{
    public class ServerResponse
    {
        public string Token { get; set; }
        public string Text { get; set; }
        public bool Success { get; set; }
    }
}