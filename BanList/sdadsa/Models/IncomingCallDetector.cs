﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Telephony;
using Android.Views;
using Android.Views.Accessibility;
using Android.Widget;
using Newtonsoft.Json;
using delrep.Activities;
using delrep.Models;
using delrep.Models.DatabaseModels;
using Environment = System.Environment;
using Intent = Android.Content.Intent;
using Path = System.IO.Path;
using TaskStackBuilder = Android.App.TaskStackBuilder;
using Uri = Android.Net.Uri;

namespace delrep
{
    [BroadcastReceiver (Name = "com.app.IncomingCallReceiver")]
    [IntentFilter(new[] { "android.intent.action.PHONE_STATE" })]
    public class IncomingCallDetector : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            if (intent.Extras != null)
            {
                string state = intent.GetStringExtra(TelephonyManager.ExtraState);
                string telephone = intent.GetStringExtra(TelephonyManager.ExtraIncomingNumber);
                string parsedNumber = telephone;
                if (state == TelephonyManager.ExtraStateRinging)
                {
                    if (!string.IsNullOrEmpty(telephone))
                    {
                        if (telephone.Length > 10)
                        {
                            parsedNumber = telephone.Substring(telephone.Length - 10, 10);
                        }
                        var dbFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                        var dbName = "context_db.db";
                        var valuesForActivity = new Bundle();
                        valuesForActivity.PutString(parsedNumber, "phone");
                        var activity = new Intent(context, typeof(PhoneInfoActivity));
                        activity.PutExtra("phone", parsedNumber);
                        var db = new CommentsDb(Path.Combine(dbFolderPath,dbName));
                            if (db.CheckPhone(parsedNumber) != null)
                            {
                                var activityIntent = new Intent(context, typeof(AlertActivity));
                                if ((((int) Build.VERSION.SdkInt <= 23)))
                                    activityIntent.SetFlags(ActivityFlags.NewTask);
                                activityIntent.PutExtra("phone", parsedNumber);
                                context.StartActivity(activityIntent);
                            }
                        
                        //var activityIntent = new Intent(context, typeof(AlertActivity));
                        //activityIntent.PutExtra("phone",telephone);
                        ////activityIntent.PutExtra("comment",comment.Text);
                        ////activityIntent.PutExtra("rating", comment.Rating);
                        ////activityIntent.PutExtra("infoBundle", info);
                        //context.StartActivity(activityIntent);
                        //var serviceIntent = new Intent(context,typeof(TestService));
                        //serviceIntent.PutExtra("phone", telephone);
                        //context.StartService(serviceIntent);

                        //context.StartForegroundService()
                        //Toast toast2 = Toast.MakeText(context, telephone, ToastLength.Long);
                        ////toast2.View.SetOnLongClickListener(Listene);
                        //toast2.Show();
                        //context.SendOrderedBroadcast(view, Intent.ActionCall);
                        //Toast toast2 = Toast.MakeText(context, "Incoming call from: " + telephone, ToastLength.Long);
                        //toast2.Show();
                        //Toast.MakeText(context, "Incoming call from " + telephone + ".", ToastLength.Short).Show();

                    }


                }
                else if (state == TelephonyManager.ExtraStateIdle)
                {
                    //context.StartActivity(typeof(MainActivity));
                }
            }
        }
    }
}