﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace delrep.Models
{
    class FormatPhone
    {
        public static string FormatNumber(string phone)
        {
            try
            {
                string result = String.Empty;
                result += "+7 (";
                result += phone.Substring(0, 3);
                result += ") ";
                result += phone.Substring(3, 3);
                result += "-";
                result += phone.Substring(6, 4);
                return result;
            }
            catch (Exception)
            {
                return phone;
            }
        }
    }
}