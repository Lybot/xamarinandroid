﻿using System.IO;
using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Support.V4.App;
using Android.Telephony;
using delrep.Activities;
using delrep.Models.DatabaseModels;
using Environment = System.Environment;
using TaskStackBuilder = Android.App.TaskStackBuilder;

namespace delrep.Models
{
    [BroadcastReceiver(Name = "com.app.IncomingCallReceiverNotification")]
    [IntentFilter(new[] { "android.intent.action.PHONE_STATE" })]
    class IncomingCallDetectorNotification:BroadcastReceiver
    {
            static readonly int NOTIFICATION_ID = 1000;
            static readonly string CHANNEL_ID = "location_notification";

        public override void OnReceive(Context context, Intent intent)
        {
            if (intent.Extras != null)
            {
                string state = intent.GetStringExtra(TelephonyManager.ExtraState);
                string telephone = intent.GetStringExtra(TelephonyManager.ExtraIncomingNumber);
                string parsedNumber = telephone;

                if (state == TelephonyManager.ExtraStateRinging)
                {
                    if (!string.IsNullOrEmpty(telephone))
                    {
                        if (telephone.Length > 10)
                        {
                            parsedNumber = telephone.Substring(telephone.Length - 10, 10);
                        }
                        var dbFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                        var dbName = "context_db.db";
                        var db = new CommentsDb(Path.Combine(dbFolderPath, dbName));
                        if (db.CheckPhone(parsedNumber) != null)
                        {
                            long[] vibrate = new long[] {1000, 1000, 1000, 1000, 1000};
                            var valuesForActivity = new Bundle();
                            valuesForActivity.PutString(parsedNumber, "phone");
                            var activity = new Intent(context, typeof(PhoneInfoActivity));
                            activity.PutExtra("phone", parsedNumber);
                            CreateNotificationChannel(context);
                            var stackBuilder = TaskStackBuilder.Create(context);
                            stackBuilder.AddNextIntent(activity);
                            var pendingIntent = stackBuilder.GetPendingIntent(0, PendingIntentFlags.UpdateCurrent);
                            var builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                                .SetAutoCancel(true)
                                .SetContentIntent(pendingIntent)
                                .SetContentTitle("Внимание звонит проблемный контрагент !")
                                .SetSmallIcon(Resource.Drawable.ic_launcher)
                                .SetContentText("Нажмите чтобы посмотреть претензии")
                                .SetVibrate(vibrate)
                                .SetSound(RingtoneManager.GetDefaultUri(RingtoneType.Notification));
                            var notifManager = NotificationManagerCompat.From(context);
                            notifManager.Notify(NOTIFICATION_ID, builder.Build());
                        }
                    }
                    //var activityIntent = new Intent(context, typeof(AlertActivity));
                    //activityIntent.PutExtra("phone",telephone);
                    ////activityIntent.PutExtra("comment",comment.Text);
                    ////activityIntent.PutExtra("rating", comment.Rating);
                    ////activityIntent.PutExtra("infoBundle", info);
                    //context.StartActivity(activityIntent);
                    //var serviceIntent = new Intent(context,typeof(TestService));
                    //serviceIntent.PutExtra("phone", telephone);
                    //context.StartService(serviceIntent);

                    //context.StartForegroundService()
                    //Toast toast2 = Toast.MakeText(context, telephone, ToastLength.Long);
                    ////toast2.View.SetOnLongClickListener(Listene);
                    //toast2.Show();
                    //context.SendOrderedBroadcast(view, Intent.ActionCall);
                    //Toast toast2 = Toast.MakeText(context, "Incoming call from: " + telephone, ToastLength.Long);
                    //toast2.Show();
                    //Toast.MakeText(context, "Incoming call from " + telephone + ".", ToastLength.Short).Show();

                }

                //Intent buttonDown = new Intent(Intent.ActionMediaButton);
                //buttonDown.PutExtra(Intent.ExtraKeyEvent, new KeyEvent(KeyEventActions.Down, Keycode.Headsethook));
                //context.SendOrderedBroadcast(buttonDown, "android.permission.CALL_PRIVILEGED");
                //context.StartActivity(buttonDown);

                //Intent buttonUp = new Intent(Intent.ActionMediaButton);
                //buttonUp.PutExtra(Intent.ExtraKeyEvent, new KeyEvent(KeyEventActions.Up, Keycode.Headsethook));
                //context.SendOrderedBroadcast(buttonUp, "android.permission.CALL_PRIVILEGED");


                else if (state == TelephonyManager.ExtraStateIdle)
                {
                    //context.StartActivity(typeof(MainActivity));
                }
            }
        }

        void CreateNotificationChannel(Context context)
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }
            var name = "topkek";
            var description = "topkok";
            var channel = new NotificationChannel(CHANNEL_ID, name, NotificationImportance.Default)
            {
                Description = description
            };
            var notificationManager = (NotificationManager)context.GetSystemService(Context.NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }
    }
}