﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace delrep.Models
{
    class User
    {
        public string Name { get; set; }
        public string Region { get; set; }
        public string FirstName { get; set; }
        public bool Premium { get; set; }
    }
}