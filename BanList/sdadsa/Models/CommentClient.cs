﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace delrep.Models
{
    public class CommentClient
    {
        public string Text { get; set; }
        public string Phone { get; set; }
        public int Debt { get; set; }
        public string Date { get; set; }
        public string Name { get; set; }
        public string CreatorPhone { get; set; }
        public string CreatorName { get; set; }
        public CommentClient(string text, string phone, int debt, string date, string name)
        {
            Text = text;
            Phone = phone;
            Debt = debt;
            Date = date;
            Name = name;
        }
        public CommentClient(string phone)
        {
            Phone = phone;
        }
        public CommentClient()
        {
        }
    }
}