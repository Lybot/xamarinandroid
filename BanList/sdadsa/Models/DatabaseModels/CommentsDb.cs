﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SQLite;


namespace delrep.Models.DatabaseModels
{
    public class CommentsDb
    {
        SQLiteConnection _database;

        public CommentsDb(string path)
        {
            _database = new SQLiteConnection(path);
            _database.CreateTable<Phones>();
        }
        public Task UpdateDb(string databasePath,string token)
        {
            Phones phone;
                try
                {
                    var allPhones = (from i in _database.Table<Phones>() select i);
                    phone = allPhones.Last();
                }
                catch (Exception)
                {
                    phone = new Phones(){Id=0};
                }
                HttpClient client = new HttpClient();
                HttpRequestMessage updateRequest = new HttpRequestMessage(HttpMethod.Post, "https://workersprojectapi.azurewebsites.net/getphones");
                updateRequest.Headers.Add("Accept", "application/json");
                updateRequest.Headers.Add("Authorization", "Bearer " + token);
                updateRequest.Content = new StringContent(JsonConvert.SerializeObject(phone),Encoding.UTF8,"application/json");
                var response = client.SendAsync(updateRequest).GetAwaiter().GetResult();
                var responseString = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                var responseObject = JsonConvert.DeserializeObject<IEnumerable<Phones>>(responseString);
            _database.InsertAll(responseObject);
            return Task.CompletedTask;
        }

        public Phones CheckPhone(string number)
        {
            Phones phone;
            try
            {
                phone = _database.Get<Phones>(check => check.Number == number);
            }
            catch (Exception)
            {
                return null;
            }
            return phone;
        }

        public IEnumerable<Phones> GetPhones()
        {
            var phones = from phone in _database.Table<Phones>() select phone;
            return phones;
        }

        public Task FullUpdateDb(string databasePath, string token)
        {
            Phones phone;
            try
            {
                var allPhones = (from i in _database.Table<Phones>() select i);
                phone = allPhones.Last();
            }
            catch (Exception)
            {
                phone = new Phones() { Id = 1 };
            }
            HttpClient client = new HttpClient();
            HttpRequestMessage updateRequest = new HttpRequestMessage(HttpMethod.Post, "https://workersprojectapi.azurewebsites.net/fullupdate");
            updateRequest.Headers.Add("Accept", "application/json");
            updateRequest.Headers.Add("Authorization", "Bearer " + token);
            updateRequest.Content = new StringContent(JsonConvert.SerializeObject(phone), Encoding.UTF8, "application/json");
            var response = client.SendAsync(updateRequest).GetAwaiter().GetResult();
            var responseString = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                var responseObject = JsonConvert.DeserializeObject<List<Phones>>(responseString);
                _database.DeleteAll<Phones>();
            _database.Commit();
                _database.InsertAll(responseObject);
                _database.Commit();
                UpdateDb(databasePath, token).GetAwaiter().GetResult();
                return Task.CompletedTask;
        }

        public Task DeleteDb()
        {
            _database.DeleteAll<Phones>();
            return Task.CompletedTask;
        }
        }
    }