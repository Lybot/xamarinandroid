﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace delrep.Models.DatabaseModels
{
    [Table("Phones")]
   public  class Phones
    {
        [PrimaryKey]
        [Unique]
        public int Id { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public int User_id { get; set; }
        public int Problem_id { get; set; }
    }
}