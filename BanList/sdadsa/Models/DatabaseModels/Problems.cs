﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace delrep.Models.DatabaseModels
{
    class Problems
    {
        public int Id { get; set; }
        public string Inn { get; set; }
        public string Name { get; set; }
    }
}