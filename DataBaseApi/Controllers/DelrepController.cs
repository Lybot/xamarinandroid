﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using DataBaseApi.Models;
using DataBaseApi.Models.delrep;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DataBaseApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DelrepController : Controller
    {
        private readonly DelrepContext _context;
        public DelrepController(DelrepContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<JsonResult> Comments([FromBody] CommentsClient comment)
        {

            return Json(comment);
        }
        [HttpGet]
        public async Task<JsonResult> Comments()
        {
            return Json("");
        }

        [HttpPost]
        public async Task<JsonResult> AddPhone([FromBody] CommentsClient comment)
        {
            return Json("");
        }
        [HttpPost]
        public async Task<JsonResult> AddName([FromBody] CommentsClient comment)
        {
            return Json("");
        }

        [HttpGet]
        public async Task<JsonResult> Problems()
        {
            var problems = await _context.Problems.ToListAsync();
            return Json(problems);
        }
        [HttpPost]
        public async Task<JsonResult> Problems([FromBody] DelrepRequest comment)
        {
            if (!String.IsNullOrWhiteSpace(comment.Inn))
                if (await _context.Problems.FirstOrDefaultAsync(check => check.Inn == comment.Inn) != null)
                    return Json(new AuthResponse("", "Данный ИНН уже зарегистрирован в базе", false));
            _context.Problems.Add(new Problems() { });
            return Json("");
        }
    }
}