﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DataBaseApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Remotion.Linq.Clauses;

namespace DataBaseApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CommentsController : Controller
    {
        private readonly WorkersDbContext _context;

        public CommentsController(WorkersDbContext context)
        {
            _context = context;
        }
        [Route("/tryconnect")]
        public async Task<JsonResult> TryConnect()
        {
            return Json(new CommentResponse() { Success = true, Text = User.Identity.Name });
        }
        public async Task<JsonResult> GetComments()
        {
            var comments =  from comment in _context.Comments
                join phone in _context.Phones on comment.Phone_id equals phone.Id
                select new CommentsClient()
                    {Phone = phone.Number, Debt = comment.Debt, Text = comment.Text,Date=comment.Date};
            return Json(comments);
        }
        [HttpPost]
        [Route("/GetComment")]
        public async Task<JsonResult> GetComment(CommentsClient phone)
        {
            var getPhone = await _context.Phones.FirstOrDefaultAsync(check => check.Number == phone.Phone);
            if (getPhone==null)
                return Json(new CommentsClient(){Phone=phone.Phone,Text="Телефон не найден"});
            var getComment = await _context.Comments.FirstOrDefaultAsync(check => check.Phone_id == getPhone.Id);
            if (getComment==null)
                return Json(new CommentsClient() { Phone = phone.Phone, Text = "Комментариев на телефон нет" });
            return Json(new CommentsClient(){Phone = getPhone.Number,Debt = getComment.Debt,Text=getComment.Text , Date = getComment.Date});
        }
        [HttpPost]
        [Route("/makecomment")]
        public async Task<JsonResult> WriteComment([FromBody] CommentsClient commentClient)
        {
            try
            {
                var creatorUser = await _context.Users.FirstOrDefaultAsync(check => check.Phone == User.Identity.Name);
                var phone = await _context.Phones.FirstOrDefaultAsync(check => commentClient.Phone == check.Number);
                if (phone != null)
                {
                    if (String.IsNullOrEmpty(phone.Name.Trim(' ')) && !String.IsNullOrEmpty(commentClient.Name.Trim(' ')))
                    {
                        phone.Name = commentClient.Name;
                        _context.Phones.Update(phone);
                    }
                    _context.Comments.Add(new Comment()
                        { Phone_id = phone.Id, Debt = commentClient.Debt, Text = commentClient.Text, Date = commentClient.Date,Creator_id = creatorUser.Id, Name = commentClient.Name});
                    await _context.SaveChangesAsync();
                    return Json(new CommentResponse() { Success = true, Text = "Комментарий успешно добавлен" });
                }
                else
                {
                    _context.Phones.Add(new Phone() { Number = commentClient.Phone, User_id = -1 ,Name=commentClient.Name});
                    await _context.SaveChangesAsync();
                    var newPhone = await _context.Phones.FirstOrDefaultAsync(check => commentClient.Phone == check.Number);
                    _context.Comments.Add(new Comment()
                        { Phone_id = newPhone.Id, Debt = commentClient.Debt, Text = commentClient.Text,Date=commentClient.Date,Creator_id = creatorUser.Id,Name = commentClient.Name});
                    await _context.SaveChangesAsync();
                    return Json(new CommentResponse() { Success = true, Text = "Комментарий успешно добавлен" });
                }
            }
            catch (Exception)
            {
                return Json(new CommentResponse() { Success = false, Text = "Произошла ошибка при обработке запроса" });
            }
        }
        [HttpPost]
        [Route("/getCommentsPhone")]
        public async Task<JsonResult> GetCommentsPhone([FromBody] Phone inputPhone)
        {
            var registeredUser = await _context.Users.FirstOrDefaultAsync(check => check.Phone == User.Identity.Name);
            var phoneCheck = await _context.Phones.FirstOrDefaultAsync(check => check.Number == inputPhone.Number);
            if (registeredUser.Premium)
            {
                IEnumerable<CommentsClient> comments = from comment in _context.Comments
                    where comment.Phone_id == phoneCheck.Id
                    join user in _context.Users on comment.Creator_id equals user.Id
                    select new CommentsClient()
                    {
                        CreatorPhone = user.Phone, Date = comment.Date, Debt = comment.Debt,
                        Text = comment.Text, Name = comment.Name, CreatorName = user.Name
                    };
                return Json(comments);
            }
            else
            {
                IEnumerable<CommentsClient> comments = from comment in _context.Comments
                    where comment.Phone_id == phoneCheck.Id && comment.Creator_id==registeredUser.Id
                    join user in _context.Users on comment.Creator_id equals user.Id
                    select new CommentsClient()
                    {
                        CreatorPhone = user.Phone,
                        Date = comment.Date,
                        Debt = comment.Debt,
                        Text = comment.Text,
                        Name = comment.Name,
                        CreatorName = user.Name
                    };
                return Json(comments);
            }

        }
        [HttpPost]
        [Route("/getphones")]
        public async Task<JsonResult> GetPhones([FromBody] Phone inputPhone)
        {
            var user = await _context.Users.FirstOrDefaultAsync(check => check.Phone == User.Identity.Name);
            if (user.Premium)
            {
                var phoneCheck = await _context.Phones.FirstOrDefaultAsync(check => check.Id == inputPhone.Id);
                if (phoneCheck == null)
                {
                    return Json(_context.Phones.ToList());
                }

                var phones = _context.Phones.ToList().Where(check => check.Id > inputPhone.Id);
                return Json(phones);
            }
            else
            {
                    var outPhones = from comments in _context.Comments
                    where comments.Creator_id==user.Id
                          join phones in _context.Phones on comments.Phone_id equals phones.Id
                                    where phones.Id >inputPhone.Id
                    select phones;
                return Json(outPhones);
            }
        }
        [HttpPost]
        [Route("/changeProfile")]
        public async Task<JsonResult> ChangeUser([FromBody] User user)
        {
            var person = await _context.Users.FirstOrDefaultAsync(checkPhone => checkPhone.Phone == User.Identity.Name);
            if (person==null)
                return Json(new AuthResponse(null, "Ошибка", false));
            person.Name = user.Name;
            person.Region = user.Region;
            try
            {
                _context.Users.Update(person);
                await _context.SaveChangesAsync();
            }
            catch(Exception)
            { return Json(new AuthResponse(null, "Ошибка", false)); }

            return Json(new AuthResponse(null, "Профиль изменен", true));
        }
        [Route("/getidentity")]
        public async Task<JsonResult> GetIdentity()
        {
            var person = await _context.Users.FirstOrDefaultAsync(check => check.Phone == User.Identity.Name);
            int divider = 0;
            try
            {
                divider = person.Name.IndexOf(" ");
            }
            catch (Exception)
            {
                divider = 0;
            }

            if (divider < 0)
                divider = 0;
            string name = String.Empty;
            string firstName = String.Empty;
            if (String.IsNullOrEmpty(person.Name))
            {
                name = "";
                firstName = "";
            }
            else
            {
                if (divider == 0)
                    name = person.Name;
                else
                {
                    firstName = person.Name.Substring(divider+1,person.Name.Length-divider-1);
                    name = person.Name.Substring(0, divider);
                }
            }
            UserClient user = new UserClient()
            {
                FirstName = firstName,
                Name = name,
                Phone = person.Phone,
                Region = person.Region,
                Premium = person.Premium
            };
            return Json(user);
        }

        [HttpPost]
        [Route("/fullUpdate")]
        public async Task<JsonResult> FullUpdate([FromBody] Phone phone)
        {
            var user = await _context.Users.FirstOrDefaultAsync(check => check.Phone == User.Identity.Name);
            if (user.Premium)
            {
                var phones = await _context.Phones.ToListAsync();
                return Json(phones);
            }
            else
            {
                var outPhones = from comments in _context.Comments
                    where comments.Creator_id == user.Id 
                    join phones in _context.Phones on comments.Phone_id equals phones.Id
                    select phones;
                return Json(outPhones.ToList());
            }
        }

        [HttpGet]
        [Route("/getApplication")]
        public async Task<JsonResult> getApplication()
        {
            try
            {
                string send ="8"+ User.Identity.Name + " Оставил заявку "; // текст SMS
                string to = "79138273743"; // номер телефона в международном формате
                //  имя отправителя из списка https://smspilot.ru/my-sender.php
                string _from = "INFORM";
                // !!! Замените API-ключ на свой https://smspilot.ru/my-settings.php#api
                string apikey = "477NV14632N6678RC6750Q2597XJ58P1PZ0XYY06ZKUP40PW577974326AVG0ICC";
                string url = "http://smspilot.ru/api.php" +
                             "?send=" + Uri.EscapeUriString(send) +
                             "&to=" + to +
                             "&from=" + _from +
                             "&apikey=" + apikey; // +
                //"&charset=windows-1251";
                HttpWebRequest myHttpWebRequest =
                    (HttpWebRequest)HttpWebRequest.Create(url);
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                return Json(new AuthResponse(null, "Ваша заявка отправлена, наш менеджер свяжется с вами в ближайшее время", true));
            }
            catch (Exception)
            {
                return Json(new AuthResponse(null, "Произошла ошибка при обработке заявки", false));
            }
        }
    }
}