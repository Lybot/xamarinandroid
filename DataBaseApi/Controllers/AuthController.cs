﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DataBaseApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
namespace DataBaseApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly WorkersDbContext _context;
        public AuthController(WorkersDbContext context)
        {
            _context = context;
        }
        [Route("auth")]
        [HttpPost]
        public async Task<JsonResult> Auth([FromBody] SmsAuth user)
        {
            try
            {
                User person = _context.Users.FirstOrDefault(check => check.Phone == user.Phone);
                //if (person == null)
                //    return base.Json(new AuthResponse(null, "Отправьте телефон заново", false));
                //if (person.Code != user.Sms)
                //    return base.Json(new AuthResponse(null, "Введен неверный код", false));
                //string send = "Был осуществлен вход с телефона : 8"+user.Phone; // текст SMS
                //string to = "79138273743"; // номер телефона в международном формате
                ////  имя отправителя из списка https://smspilot.ru/my-sender.php
                //string _from = "INFORM";
                //// !!! Замените API-ключ на свой https://smspilot.ru/my-settings.php#api
                //string apikey = "477NV14632N6678RC6750Q2597XJ58P1PZ0XYY06ZKUP40PW577974326AVG0ICC";
                //string url = "http://smspilot.ru/api.php" +
                //             "?send=" + Uri.EscapeUriString(send) +
                //             "&to=" + to +
                //             "&from=" + _from +
                //             "&apikey=" + apikey; // +
                //"&charset=windows-1251";

                //HttpWebRequest myHttpWebRequest =
                //    (HttpWebRequest)HttpWebRequest.Create(url);
                //HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                var identity = await GetIdentityAsync(person);
                var now = DateTime.UtcNow;
                var jwt = new JwtSecurityToken(
                    AuthOptions.ISSUER,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                        SecurityAlgorithms.HmacSha256));
                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                return Json(new AuthResponse(encodedJwt, person.Phone, true));
            }
            catch (Exception e)
            {
                return Json(new AuthResponse(null, e.Message, false));
            }
        }
        private async Task<ClaimsIdentity> GetIdentityAsync(User person)
        {
            if (person != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.Phone),
                };
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
            return null;
        }
        [HttpPost("/register")]
        public async Task<JsonResult> GetSms([FromBody]  SmsAuth phone)
        {
            if (phone.Phone.Length != 10)
                return Json(new AuthResponse(null, "Некорректные данные", false));
            var random = new Random();
            string code = String.Empty;
            for (int i = 0; i < 4; i++)
            {
                int codeInt = random.Next(0, 9);
                code += (codeInt.ToString());
            }
            if (_context.Users.Any(check => check.Phone == phone.Phone))
            {
                var user = await _context.Users.FirstOrDefaultAsync(check => check.Phone == phone.Phone);
                user.Code = code;
                _context.Users.Update(user);
                await _context.SaveChangesAsync();
            }
            else
            {
                var user = new User() {Phone = phone.Phone, Code = code,Premium = true,Date=DateTime.Now.ToShortDateString()};
                await _context.Users.AddAsync(user);
                await _context.SaveChangesAsync();
            }
            //string send = "Для подтверждения телефона введите код " + code; // текст SMS
            //string to = "7" + phone.Phone; // номер телефона в международном формате
            ////  имя отправителя из списка https://smspilot.ru/my-sender.php
            //string _from = "INFORM";
            //// !!! Замените API-ключ на свой https://smspilot.ru/my-settings.php#api
            //string apikey = "477NV14632N6678RC6750Q2597XJ58P1PZ0XYY06ZKUP40PW577974326AVG0ICC";
            //string url = "http://smspilot.ru/api.php" +
            //             "?send=" + Uri.EscapeUriString(send) +
            //             "&to=" + to +
            //             "&from=" + _from +
            //             "&apikey=" + apikey; // +
            ////"&charset=windows-1251";

            //HttpWebRequest myHttpWebRequest =
            //    (HttpWebRequest)HttpWebRequest.Create(url);
            //HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            return Json(new AuthResponse(null, "Код отправлен", true));
        }
        //// DELETE: api/Auth/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteUser([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var user = await _context.Users.FindAsync(id);
        //    if (user == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Users.Remove(user);
        //    await _context.SaveChangesAsync();

        //    return Ok(user);
        //}
    }
}