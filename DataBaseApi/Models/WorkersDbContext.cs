﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DataBaseApi.Models
{
    public class WorkersDbContext:DbContext
    {
        public WorkersDbContext(DbContextOptions<WorkersDbContext> options) : base(options)
        {
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Phone> Phones { get; set; }
    }
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new WorkersDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<WorkersDbContext>>()))
            {
                if (context.Users.Any() && context.Comments.Any() && context.Phones.Any())
                {
                    return;   // DB has been seeded
                }
                if (!context.Users.Any())
                    context.Users.Add(
                     new User
                     {
                         Phone = "9609129404",
                         Name = "amin",
                         Premium = true,
                         Region = "Tomsk"
                     }
                );
                if (!context.Comments.Any())
                    context.Comments.Add(
                        new Comment
                        {
                            Text = "9609129404",
                            Debt = 5,
                            Phone_id = 1,
                            Creator_id = 1,
                            Date = DateTime.Now.ToShortDateString()
                        }
                    );
                if (!context.Phones.Any())
                    context.Phones.Add(
                        new Phone
                        {
                            Number = "9609129404",
                            User_id = 1
                        }
                    );
                context.SaveChanges();
            }
        }
    }
}

