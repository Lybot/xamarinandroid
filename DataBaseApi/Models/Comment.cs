﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int Debt { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public int Phone_id { get; set; }
        public int Creator_id { get; set; }
    }
}
