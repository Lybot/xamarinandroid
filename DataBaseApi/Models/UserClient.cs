﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models
{
    public class UserClient
    {
        public string Phone { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string Region { get; set; }
        public bool Premium { get; set; }
    }
}
