﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models
{
    public class AuthResponse
    {
        public string Token { get; set; }
        public string Text { get; set; }
        public bool Success { get; set; }

        public AuthResponse(string Token, string Text, bool Success)
        {
            this.Token = Token;
            this.Text = Text;
            this.Success = Success;
        }
    }
}
