﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models
{
    public class CommentsClient
    {
        public string Text { get; set; }
        public int Debt { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Date { get; set; }
        public string CreatorPhone { get; set; }
        public string CreatorName { get; set; }
    }
}
