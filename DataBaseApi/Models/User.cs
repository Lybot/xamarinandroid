﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Premium { get; set; }
        public string Region { get; set; }
        public string Date { get; set; }
    }
}
