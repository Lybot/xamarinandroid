﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models.delrep
{
    public class Comments
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int Debt { get; set; }
        public DateTime Date { get; set; }
        public int Problem_id { get; set; }
        public string File { get; set; }
        public int Creator_id { get; set; }
    }
}
