﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models.delrep
{
    public class Names
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Problem_id { get; set; }
    }
}
