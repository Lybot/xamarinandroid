﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DataBaseApi.Models.delrep
{
    public class DelrepContext:DbContext
    {
        public DelrepContext(DbContextOptions<DelrepContext> options) : base(options)
        {
        }
        public DbSet<Comments> Comments { get; set; }
        public DbSet<Names> Names { get; set; }
        public DbSet<Phones> Phones { get; set; }
        public DbSet<Problems> Problems { get; set; }
    }
    public static class SeedDelrep
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new DelrepContext(
                serviceProvider.GetRequiredService<DbContextOptions<DelrepContext>>()))
            {           
                context.SaveChanges();
            }
        }
    }
}
