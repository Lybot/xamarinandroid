﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models.delrep
{
    public class DelrepRequest
    {
        public int Id { get; set; }
        public List<String> Phones { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public int Debt { get; set; }
        public string Inn { get; set; }
    }
}
