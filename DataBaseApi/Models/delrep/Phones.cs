﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models.delrep
{
    public class Phones
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public int User_id { get; set; }
        public int Problem_id { get; set; }
    }
}
