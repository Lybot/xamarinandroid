﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models.delrep
{
    public class Problems
    {
        public int Id { get; set; }
        public string Inn { get; set; }
        public string Name { get; set; }
    }
}
