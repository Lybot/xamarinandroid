﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models
{
    public class CommentResponse
    {
        public string Text { get; set; }
        public bool Success { get; set; }
        //public string Text { get; set; }
    }
}
