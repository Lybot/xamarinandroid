﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseApi.Models
{
    public class SmsAuth
    {
        public string Sms { get; set; }
        public string Phone { get; set; }
    }
}
